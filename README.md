# TS-simulace

Semestralní práce z TS. Testování real time simulace

Použité knihovny:

Simulace byla vyvinuta pomocí knihovny Swing, lock frameworku.
Junit a Mockito pro testy.

Třídy:

Entity v simulace jsou založeny na abstraktní třídě Entity.java
Každé vlakno je založeno na své třídě, ve formatu EntitaRunnable.java
Statistika běží na třídě StatisticsRunnable.java
GUI běží na třídě GridMap.java, zobrazuje mapu v každém okamžiku

Koncept:

Řídí okna třída BoardManager.
Samotné okno simulace je vytvářeno třídou GridMap.java, která rozšiřuje JPanel, a implementuje rozhrání Runnable.
Zpracování událostí stisku tlačítek z klávesnice je realizováno třídou GameListener.java.
ParametrsView - zobrazuje okno pro přidavaní a editace entit, které budou simulovany.
StatisticsView - aktualizuje statistiky v realném času.


Entity jsou ovládáni pomocí vláken. Konkrétně třídami formatu *Runnable.java. Dále je vlákno použito pro asynchronní simulaci jednotlivých entit.

Vnitřní průběh programu

Program začíná ve funkci setEntities, kde uživatel může zadat počet a parametry objektů. Simulace se spustí, každe vlakno začně svou práci
Každé Runnable Třída simuluje model své entity, t.j mění pozice, zdráví, hlad atd.

Stav entity:
"isAlive";

Stav simulace: 
"isRunning";
